import { selec } from './../seleccion-interface/seleccion.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-seleccion',
  templateUrl: './seleccion.component.html',
  styleUrls: ['./seleccion.component.css']
})
export class SeleccionComponent implements OnInit {

  seleccion: selec = {
    aceptar: false,
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar(): void {
    console.log(this.seleccion.aceptar);
    
  }

}
